<?php

use App\Http\Controllers\Api\ZohoController;
use Illuminate\Support\Facades\Route;


Route::post('/zohocrm/set-token', [ZohoController::class, 'setToken']);
Route::post('/zohocrm/create-deal', [ZohoController::class, 'createDeal']);
Route::get('/zohocrm/get-stages', [ZohoController::class, 'getStages']);
