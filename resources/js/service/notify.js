import { Notify } from 'quasar';
export default {
    notify(message, bgColor, textColor){
        Notify.create({
            message: message,
            position: 'top-left',
            color:bgColor,
            textColor:textColor,
            actions: [
                {
                    icon: 'close', color: 'white', round: true, handler: () => {}
                }
            ]
        });
    }
}
