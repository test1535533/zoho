import {createWebHistory, createRouter} from 'vue-router'
import store from '../store';

const routes = [
    {
        path: '/',
        component: () => import( '../pages/MainPage.vue'),
        name: 'home',
    },
]
export default createRouter({
    history: createWebHistory(),
    routes,
})

