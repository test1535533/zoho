import axios from "axios";
// const origin = `${env.VUE_APP_API_URL}/api`;
const routes = {
    get: {
        getStages: `api/zohocrm/get-stages`,
    },
    post: {
        createDeal: `api/zohocrm/create-deal`,
    },
};

export default {
    getRoutes()  {
        return routes
    },
    getStages(header ){
        const url = routes.get.getStages;
        return axios.get(url, header);
    },
    createDeal(data,header ){
        const url = routes.post.createDeal;
        return axios.post(url, data, header);
    },
}
