import * as types from '../mutayion-types';
import apiDeals from "../../api/deals"
import notifyService from "../../service/notify";

const state = {
    message: '',
    stages: [],
    errors: []
};

const getters = {
    stages: state => state.stages,
    errors: state => state.errors,
    message: state => state.message,
};

const actions = {
    async getStages(context) {
        await apiDeals.getStages( {})
            .then((response) => {
                context.commit(types.SET_STAGES,response.data.data );
            })
            .catch(err => {
                context.commit(types.SET_MESSAGE, err.response.data.message);
                context.commit(types.SET_ERRORS, err.response.data.errors);
                if (err) {
                    notifyService.notify(err.response.data.message, 'orange', 'white')
                }
            });
    },
    async createDeal(context, deal) {
        let formData = new FormData();
        formData.append('Account_Name', deal.Account_Name);
        formData.append('Website', deal.Website);
        formData.append('Phone', deal.Phone);
        formData.append('Deal_Name', deal.Deal_Name);
        formData.append('Stage', deal.Stage);
        await apiDeals.createDeal(formData, {
            headers: {'Content-Type': 'multipart/form-data'},
        })
            .then((response) => {
                context.commit(types.SET_DELETE_MESSAGE);
                context.commit(types.SET_DELETE_ERRORS);
                if (response) {
                    notifyService.notify(response.data.message, 'teal', 'white')
                }
            })
            .catch(err => {
                context.commit(types.SET_MESSAGE, err.response.data.message);
                context.commit(types.SET_ERRORS, err.response.data.errors);
                if (err) {
                    notifyService.notify(err.response.data.message, 'orange', 'white')
                }
            });
    },
    async clearMessage(context) {
        await context.commit(types.SET_DELETE_MESSAGE);
    },
    async clearErrors(context) {
        await context.commit(types.SET_DELETE_ERRORS);
    }
};


const mutations = {
    [types.SET_MESSAGE]: (state, message) => {
        state.message = message
    },
    [types.SET_STAGES]: (state, stages) => {
        state.stages = stages
    },
    [types.SET_DELETE_MESSAGE]: (state) => {
        state.message = ''
    },
    [types.SET_ERRORS]: (state, errors) => {
        console.log(errors)
        state.errors = errors
    },
    [types.SET_DELETE_ERRORS]: (state) => {
        state.errors = []
    },
};


export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}

