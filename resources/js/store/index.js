import {createStore} from 'vuex'

import deals from './modules/deals'
export default createStore({
    modules: {
        deals
    }
})
