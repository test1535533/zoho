export const SET_DELETE_MESSAGE ='SET_DELETE_MESSAGE'
export const SET_DELETE_ERRORS ='SET_DELETE_ERRORS'
export const SAVE_NEW_DEAL ='SAVE_NEW_DEAL'
export const SET_ERRORS ='SET_ERRORS'
export const SET_MESSAGE ='SET_MESSAGE'
export const SET_STAGES ='SET_STAGES'

