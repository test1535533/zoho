import './bootstrap';
import {createApp} from 'vue'
import App from './App.vue'
import router from './router/index';
import store from './store/index'
import { Quasar } from 'quasar'
import 'quasar/src/css/index.sass'
import '@quasar/extras/material-icons/material-icons.css'
import quasarUserOptions from './quasar-user-options'

const app = createApp(App).use(Quasar, quasarUserOptions)
app.use(router)
app.use(store)
app.mount('#app')
