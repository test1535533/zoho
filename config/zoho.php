<?php
return [

    'client_id' => env('ZOHO_CLIENT_ID'),
    'client_secret' => env('ZOHO_CLIENT_SECRET'),
    'redirect_uri' => env('ZOHO_REDIRECT_URI'),
    'current_user_email' => env('ZOHO_USER_EMAIL'),
    'sandbox' => env('ZOHO_SANDBOX', false),
    'application_log_file_path' => storage_path('logs/zoho_sdk.log'),
    'autoRefreshFields' => true,

];
