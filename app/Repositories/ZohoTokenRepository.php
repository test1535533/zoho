<?php

namespace App\Repositories;

use App\Interfaces\Repositories\ZohoTokenRepositoryInterface;
use App\Models\ZohoToken;

class ZohoTokenRepository implements ZohoTokenRepositoryInterface
{
    public function getToken(): ZohoToken|null
    {
        return ZohoToken::firstOrFail();
    }

    /**
     * @param array $data
     * @return ZohoToken
     */
    public function createToken(array $data): ZohoToken
    {
        return ZohoToken::create($data);
    }

    /**
     * @param ZohoToken $zohoToken
     * @param array $data
     * @return ZohoToken
     */
    public function updateToken(ZohoToken $zohoToken, array $data): ZohoToken
    {
        $zohoToken->update($data);
        return $zohoToken->refresh();
    }
}
