<?php

namespace App\Repositories;

use App\Interfaces\Repositories\ZohoRepositoryInterface;
use App\Interfaces\Repositories\ZohoTokenRepositoryInterface;
use App\Models\Account;
use App\Models\Deal;
use App\Models\ZohoToken;

class ZohoRepository implements ZohoRepositoryInterface
{
    public function getAccountByName(string $account_name): Account|null
    {
        return Account::where('account_name', '=', $account_name)->first();
    }

    /**
     * @param array $data
     * @return Account
     */
    public function createAccount(array $data): Account
    {
        return Account::create($data);
    }

    /**
     * @param array $data
     * @return Deal
     */
    public function createDeal(array $data): Deal
    {
        return Deal::create($data);
    }
}
