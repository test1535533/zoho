<?php

namespace App\Providers;

use App\Interfaces\Services\HttpServiceInterface;
use App\Interfaces\Services\ZohoApiUrlServiceInterface;
use App\Interfaces\Services\ZohoServiceInterface;
use App\Interfaces\Services\ZohoTokenServiceInterface;
use App\Services\HttpService;
use App\Services\ZohoApiUrlService;
use App\Services\ZohoService;
use App\Services\ZohoTokenService;
use Illuminate\Support\ServiceProvider;


class ServicesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register():void
    {
        $this->app->bind(ZohoServiceInterface::class,ZohoService::class);
        $this->app->bind(ZohoTokenServiceInterface::class,ZohoTokenService::class);
        $this->app->bind(ZohoApiUrlServiceInterface::class,ZohoApiUrlService::class);
        $this->app->bind(HttpServiceInterface::class,HttpService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot():void
    {
        //
    }
}
