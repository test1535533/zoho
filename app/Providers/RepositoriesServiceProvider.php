<?php

namespace App\Providers;

use App\Interfaces\Repositories\AuthRepositoryInterface;
use App\Interfaces\Repositories\FileRepositoryInterface;
use App\Interfaces\Repositories\ZohoRepositoryInterface;
use App\Interfaces\Repositories\ZohoTokenRepositoryInterface;
use App\Repositories\AuthRepository;
use App\Repositories\FileRepository;
use App\Repositories\ZohoRepository;
use App\Repositories\ZohoTokenRepository;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register():void
    {
        $this->app->bind(ZohoTokenRepositoryInterface::class,ZohoTokenRepository::class);
        $this->app->bind(ZohoRepositoryInterface::class,ZohoRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot():void
    {
        //
    }
}
