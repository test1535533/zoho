<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Department
 *
 * @property int $id
 * @property int $expires_in
 * @property string $refresh_token
 * @property string $access_token
 * @property string $client_id
 * @property string $client_secret
 * @property string $api_domain
 * @property mixed $updated_at
 * @method static updateOrCreate(array $array, array $data)
 */
class ZohoToken extends Model
{
    use HasFactory;

    protected $fillable = [
        'refresh_token',
        'access_token',
        'client_id',
        'client_secret',
        'api_domain',
        'expires_in',
    ];
}
