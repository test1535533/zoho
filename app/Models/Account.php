<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Department
 *
 * @property int $id
 * @property string $account_name
 * @property string $crm_account_id
 * @method static create(array $data)
 * @method static where(string $string, string $string1, string $account_name)
 */
class Account extends Model
{
    use HasFactory;

    protected $fillable = [
        'account_name',
        'crm_account_id',
    ];

    /**
     * @return HasMany
     */
    public function deals(): HasMany
    {
        return $this->hasMany(Deal::class);
    }
}
