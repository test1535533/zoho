<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Department
 *
 * @property int $id
 * @property int $account_id
 * @property string $deal_name
 * @property string $crm_account_id
 * @property string $crm_deal_id
 * @property Account $account
 * @method static create(array $data)
 */
class Deal extends Model
{
    use HasFactory;

    static array $_STAGE = [
        'Оценка пригодности',
        'Требуется анализ',
        'Ценностное предложение',
        'Идентификация ответственных за принятие решений',
        'Коммерческое предложение/Ценовое предложение',
        'Переговоры /Оценка',
        'Закрытые заключенные',
        'Identify Decision Makers',
        'Закрытые заключенные конкурентами',
    ];

    protected $fillable = [
        'account_id',
        'crm_account_id',
        'deal_name',
        'crm_deal_id',
    ];

    /**
     * @return BelongsTo
     */
    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class);
    }

}
