<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateDealRequest
 *
 * @property string $Account_Name
 * @property string $Website
 * @property string $Phone
 * @property string $Deal_Name
 * @property string $Stage
 *
 * @package App\Http\Requests
 */
class CreateDealRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules():array
    {
        return [
            'Account_Name' => 'required|unique:accounts',
            'Website' => 'required|url|starts_with:https://',
            'Phone' => 'required|string',
            'Deal_Name' => 'required|string',
            'Stage' => 'required|string',
        ];
    }
}
