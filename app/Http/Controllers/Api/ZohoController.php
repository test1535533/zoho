<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CreateDealRequest;
use App\Http\Requests\SetTokenRequest;
use App\Http\Resources\DealResource;
use App\Interfaces\Services\ZohoServiceInterface;
use App\Interfaces\Services\ZohoTokenServiceInterface;
use App\Models\Deal;
use App\Responses\ReportMessageResponse;
use App\Responses\ReportMessageResponseError;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ZohoController extends ApiController
{
    private ZohoServiceInterface $zohoService;
    private ZohoTokenServiceInterface $zohoTokenService;

    /**
     * @param ZohoServiceInterface $zohoService
     * @param ZohoTokenServiceInterface $zohoTokenService
     */
    public function __construct(ZohoServiceInterface $zohoService, ZohoTokenServiceInterface $zohoTokenService)
    {
        $this->zohoService = $zohoService;
        $this->zohoTokenService = $zohoTokenService;
    }


    /**
     * @OA\Post  (path="/api/zohocrm/create-deal",tags={"ZohoCrm"},summary="Create Deal", operationId="Create",
     *     @OA\RequestBody(
     *          @OA\MediaType(mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(property="Account_Name",type="string"),
     *                  @OA\Property(property="Website",type="string"),
     *                  @OA\Property(property="Phone",type="string"),
     *                  @OA\Property(property="Deal_Name",type="string"),
     *                  @OA\Property(property="Stage",type="string", enum={"Оценка пригодности", "Требуется анализ", "Ценностное предложение",
     *                               "Идентификация ответственных за принятие решений","Коммерческое предложение/Ценовое предложение","Переговоры /Оценка","Закрытые заключенные","Identify Decision Makers","Закрытые заключенные конкурентами"}),
     *                  required={"Account_Name","Deal_Name","Stage","Website","Phone"}))),
     *   @OA\Response(response=200,description="The function set token",@OA\JsonContent(
     *              @OA\Property(property="success",type="boolean", example="true"),
     *              @OA\Property(property="message",type="string", example="Create deal"),
     *              @OA\Property(property="data",type="array",@OA\Items(type="object",
     *                      @OA\Property(property="refresh_token",type="string"),
     *                      @OA\Property(property="access_token",type="string"),
     *                      @OA\Property(property="client_id",type="string"),
     *                      @OA\Property(property="client_secret",type="string"),
     *                      @OA\Property(property="api_domain",type="string"),
     *                      @OA\Property(property="expires_in",type="integer"),
     *              )),
     *              @OA\Property(property="statusCode",type="integer", example="200")),
     *   ),
     *   @OA\Response(response=500,description="Internal Server Error", @OA\JsonContent(
     *              @OA\Property(property="success",type="boolean",example="false"),
     *              @OA\Property(property="message",type="string",example="Internal Server Error"),
     *              @OA\Property(property="errors",type="array",@OA\Items(type="object")),
     *              @OA\Property(property="statusCode",type="string",example="500")),
     *   )
     * )
     * @param CreateDealRequest $request
     * @return JsonResponse
     */
    public function createDeal(CreateDealRequest $request): JsonResponse
    {
        try {
            $account = $this->zohoService->createAccount($request);
            $deal = $this->zohoService->createDeal($request, $account);
            $updateDeal = $this->zohoService->updateDeal($deal);
            $response = response()->json(new ReportMessageResponse(true, 'Create deal successfully', new DealResource($updateDeal), ResponseAlias::HTTP_OK));
        } catch (Exception $e) {
            return $this->makeResponse(new ReportMessageResponseError(false, 'Something is wrong', $e->getMessage(), 500));
        }
        return $response;
    }
    /**
     * @OA\Get  (path="/api/zohocrm/get-stages",tags={"ZohoCrm"},summary="Get Stages", operationId="GetSteges",
     *   @OA\Response(response=200,description="The function set token",@OA\JsonContent(
     *              @OA\Property(property="success",type="boolean", example="true"),
     *              @OA\Property(property="message",type="string", example="Get Stages"),
     *              @OA\Property(property="data",type="array",@OA\Items(type="object")),
     *              @OA\Property(property="statusCode",type="integer", example="200")),
     *   ),
     *   @OA\Response(response=500,description="Internal Server Error", @OA\JsonContent(
     *              @OA\Property(property="success",type="boolean",example="false"),
     *              @OA\Property(property="message",type="string",example="Internal Server Error"),
     *              @OA\Property(property="errors",type="array",@OA\Items(type="object")),
     *              @OA\Property(property="statusCode",type="string",example="500")),
     *   )
     * )
     * @return JsonResponse
     */
    public function getStages(): JsonResponse
    {
        try {
            $stage=Deal::$_STAGE;
            $response = response()->json(new ReportMessageResponse(true, 'Get all stage', $stage, ResponseAlias::HTTP_OK));
        } catch (Exception $e) {
            return $this->makeResponse(new ReportMessageResponseError(false, 'Something is wrong', $e->getMessage(), 500));
        }
        return $response;
    }

    /**
     * @OA\Post  (path="/api/zohocrm/set-token",tags={"ZohoCrm"},summary="Set Token Zoho CRM", operationId="SetToken",
     *     @OA\RequestBody(
     *          @OA\MediaType(mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(property="client_id",type="string"),
     *                  @OA\Property(property="client_secret",type="string"),
     *                  @OA\Property(property="redirect_uri",type="string"),
     *                  @OA\Property(property="code",type="string"),
     *                  required={"client_id","client_secret","redirect_uri","code" }))),
     *   @OA\Response(response=200,description="The function set token",@OA\JsonContent(
     *              @OA\Property(property="success",type="boolean", example="true"),
     *              @OA\Property(property="message",type="string", example="Set token successfully"),
     *              @OA\Property(property="data",type="array",@OA\Items(type="object",
     *                      @OA\Property(property="refresh_token",type="string"),
     *                      @OA\Property(property="access_token",type="string"),
     *                      @OA\Property(property="client_id",type="string"),
     *                      @OA\Property(property="client_secret",type="string"),
     *                      @OA\Property(property="api_domain",type="string"),
     *                      @OA\Property(property="expires_in",type="integer"),
     *              )),
     *              @OA\Property(property="statusCode",type="integer", example="200")),
     *   ),
     *   @OA\Response(response=500,description="Internal Server Error", @OA\JsonContent(
     *              @OA\Property(property="success",type="boolean",example="false"),
     *              @OA\Property(property="message",type="string",example="Internal Server Error"),
     *              @OA\Property(property="errors",type="array",@OA\Items(type="object")),
     *              @OA\Property(property="statusCode",type="string",example="500")),
     *   )
     * )
     * @param SetTokenRequest $request
     * @return JsonResponse
     */
    public function setToken(SetTokenRequest $request): JsonResponse
    {

        try {
            $response = Http::asForm()->post('https://accounts.zoho.eu/oauth/v2/token', ['grant_type' => 'authorization_code',
                'client_id' => $request->client_id,
                'client_secret' => $request->client_secret,
                'redirect_uri' => $request->redirect_uri,
                'code' => $request->code]);
            $data = $response->json();
            $token = $this->zohoTokenService->createToken([
                'refresh_token' => $data['refresh_token'],
                'access_token' => $data['access_token'],
                'client_id' => $request->client_id,
                'client_secret' => $request->client_secret,
                'api_domain' => $data['api_domain'],
                'expires_in' => $data['expires_in'],
            ]);
            $response = response()->json(new ReportMessageResponse(true, 'Set token successfully', $token, ResponseAlias::HTTP_OK));
        } catch (Exception $e) {
            $response = response()->json(new ReportMessageResponseError(false, $e->getMessage(), null, ResponseAlias::HTTP_INTERNAL_SERVER_ERROR));
        }
        return $response;
    }
}
