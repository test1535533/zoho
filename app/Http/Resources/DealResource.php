<?php

namespace App\Http\Resources;

use App\Models\Deal;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DealResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request):array
    {

        /**
         * @var Deal $this
         */
        return  [
            'id' => $this->id,
            'dealName' => $this->deal_name,
            'accountName' => $this->account->account_name,
        ];
    }
}
