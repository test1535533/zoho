<?php

namespace App\Http\Resources;

use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request):array
    {

        /**
         * @var Review $this
         */
        return  [
            'id' => $this->id,
            'reviewer' => $this->reviewer,
            'email' => $this->email,
            'review' => $this->review,
            'rating' => $this->rating,
            'employee' => $this->employee,
            'employeesPosition' => $this->employees_position,
            'uniqueEmployeeNumber' => $this->unique_employee_number,
            'company' => $this->company,
            'companyDescription' => $this->company_description,
        ];
    }
}
