<?php
namespace App\Interfaces\Services;

interface ZohoApiUrlServiceInterface
{
    /**
     * @return string
     */
    public function setToken(): string;

    /**
     * @param string $refresh_token
     * @param string $client_id
     * @param string $client_secret
     * @return string
     */
    public function refreshToken(string $refresh_token, string $client_id, string $client_secret): string;

    /**
     * @return string
     */
    public function account(): string;

    /**
     * @return string
     */
    public function deals(): string;
}
