<?php
namespace App\Interfaces\Services;

use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response;

interface HttpServiceInterface
{
    /**
     * @param string $method
     * @param string $url
     * @param array $data
     * @return PromiseInterface|Response
     */
    public function zohoRequest(string $method, string $url,array $data):PromiseInterface|Response;

}
