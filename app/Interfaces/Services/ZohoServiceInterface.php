<?php

namespace App\Interfaces\Services;


use App\Http\Requests\CreateDealRequest;
use App\Models\Account;
use App\Models\Deal;

interface ZohoServiceInterface
{
    /**
     * @param CreateDealRequest $request
     * @return Account
     */
    public function createAccount( CreateDealRequest $request):Account;

    /**
     * @param CreateDealRequest $request
     * @param Account $account
     * @return Deal
     */
    public function createDeal(CreateDealRequest $request, Account $account):Deal;

    /**
     * @param Deal $deal
     * @return Deal
     */
    public function updateDeal(Deal $deal): Deal;

}
