<?php
namespace App\Interfaces\Repositories;

use App\Models\ZohoToken;

interface ZohoTokenRepositoryInterface
{
    /**
     * @return ZohoToken|null
     */
    public function getToken(): ZohoToken|null;


    /**
     * @param array $data
     * @return ZohoToken
     */
    public function createToken(array $data): ZohoToken;

    /**
     * @param ZohoToken $zohoToken
     * @param array $data
     * @return ZohoToken
     */
    public function updateToken(ZohoToken $zohoToken, array $data): ZohoToken;
}
