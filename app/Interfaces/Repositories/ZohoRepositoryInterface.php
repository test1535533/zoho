<?php
namespace App\Interfaces\Repositories;


use App\Models\Account;
use App\Models\Deal;

interface ZohoRepositoryInterface
{
    /**
     * @param string $account_name
     * @return Account|null
     */
    public function getAccountByName(string $account_name): Account|null;

    /**
     * @param array $data
     * @return Account
     */
    public function createAccount(array $data):Account;

    /**
     * @param array $data
     * @return Deal
     */
    public function createDeal(array $data): Deal;

}
