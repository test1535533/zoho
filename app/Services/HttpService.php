<?php


namespace App\Services;

use App\Interfaces\Repositories\ZohoTokenRepositoryInterface;
use App\Interfaces\Services\HttpServiceInterface;
use App\Interfaces\Services\ZohoApiUrlServiceInterface;
use App\Interfaces\Services\ZohoTokenServiceInterface;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;

class HttpService implements HttpServiceInterface
{
    private ZohoTokenServiceInterface $zohoTokenService;
    private ZohoApiUrlServiceInterface $zohoApiUrlService;

    /**
     * UserService constructor.
     * @param ZohoTokenServiceInterface $zohoTokenService
     * @param ZohoApiUrlServiceInterface $zohoApiUrlService
     */
    public function __construct(ZohoTokenServiceInterface $zohoTokenService, ZohoApiUrlServiceInterface $zohoApiUrlService)
    {
        $this->zohoTokenService = $zohoTokenService;
        $this->zohoApiUrlService = $zohoApiUrlService;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $data
     * @return PromiseInterface|Response
     */
    public function zohoRequest(string $method, string $url, array $data): PromiseInterface|Response
    {
        $zohoToken = $this->zohoTokenService->getToken();
        $now = Carbon::now();
        $diffInSeconds = $now->diffInSeconds($zohoToken->updated_at);
        if ($diffInSeconds > $zohoToken->expires_in) {
            $dataStore = $this->refreshToken();
            $zohoToken = $this->zohoTokenService->updateToken($zohoToken, $dataStore);
        }
        $token = $zohoToken->access_token;
        $client = Http::withHeaders([
            'Authorization' => 'Zoho-oauthtoken ' . $token
        ]);

        return match ($method) {
            'GET' => $client->get($url, $data),
            'POST' => $client->post($url, $data),
            'PATH' => $client->patch($url, $data),
        };
    }

    /**
     * @return array
     */
    private function refreshToken(): array
    {
        $zohoToken = $this->zohoTokenService->getToken();
        $refreshUrl = $this->zohoApiUrlService->refreshToken($zohoToken->refresh_token, $zohoToken->client_id, $zohoToken->client_secret);
        $response = Http::asForm()->post($refreshUrl);
        $data = $response->json();
        return [
            'access_token' => $data['access_token'],
            'api_domain' => $data['api_domain'],
            'expires_in' => $data['expires_in']
        ];
    }
}
