<?php

namespace App\Services;

use App\Http\Requests\CreateDealRequest;
use App\Interfaces\Repositories\ZohoRepositoryInterface;
use App\Interfaces\Repositories\ZohoTokenRepositoryInterface;
use App\Interfaces\Services\HttpServiceInterface;
use App\Interfaces\Services\ZohoApiUrlServiceInterface;
use App\Interfaces\Services\ZohoServiceInterface;
use App\Interfaces\Services\ZohoTokenServiceInterface;
use App\Models\Account;
use App\Models\Deal;
use App\Models\ZohoToken;

class ZohoService implements ZohoServiceInterface
{
    private ZohoRepositoryInterface $zohoRepository;
    private HttpServiceInterface $httpService;
    private ZohoApiUrlServiceInterface $zohoApiUrlService;


    /**
     * UserService constructor.
     * @param ZohoRepositoryInterface $zohoRepository
     * @param HttpServiceInterface $httpService
     * @param ZohoApiUrlServiceInterface $zohoApiUrlService
     */
    public function __construct(ZohoRepositoryInterface $zohoRepository, HttpServiceInterface $httpService, ZohoApiUrlServiceInterface $zohoApiUrlService)
    {
        $this->zohoRepository = $zohoRepository;
        $this->httpService = $httpService;
        $this->zohoApiUrlService = $zohoApiUrlService;
    }

    /**
     * @param CreateDealRequest $request
     * @return Account
     */
    public function createAccount(CreateDealRequest $request): Account
    {
        $url = $this->zohoApiUrlService->account();
        $data = [
            'data' => [
                [
                    'Account_Name' => $request->Account_Name,
                    "Website" => $request->Website,
                    "Phone" => $request->Phone
                ],
            ],
        ];
        $responseAccount = $this->httpService->zohoRequest('POST', $url, $data);
        $dataAccount = $responseAccount->json();
        $data = ['account_name' => $request->Account_Name, 'crm_account_id' => $dataAccount['data'][0]['details']['id']];
        return $this->zohoRepository->createAccount($data);
    }

    /**
     * @param CreateDealRequest $request
     * @param Account $account
     * @return Deal
     */
    public function createDeal(CreateDealRequest $request, Account $account): Deal
    {
        $url = $this->zohoApiUrlService->deals();
        $data = [
            'data' => [
                [
                    'Deal_Name' => $request->Deal_Name,
                    'Stage' => $request->Stage,
                ],
            ],
        ];
        $responseDeal = $this->httpService->zohoRequest('POST', $url, $data);
        $dataDeal = $responseDeal->json();
        $deal_id = $dataDeal['data'][0]['details']['id'];
        $data = ['deal_name' => $request->Deal_Name, 'crm_deal_id' => $deal_id, 'account_id' => $account->id, 'crm_account_id' => $account->crm_account_id];
        return $this->zohoRepository->createDeal($data);
    }

    public function updateDeal(Deal $deal): Deal
    {
        $url = $this->zohoApiUrlService->deals();
        $data = [
            'data' => [
                [
                    "Account_Name" => [
                        "id" => $deal->crm_account_id
                    ]
                ]
            ]
        ];
        $this->httpService->zohoRequest('PATH', $url . '/' . $deal->crm_deal_id, $data);
        return $deal;
    }
}
