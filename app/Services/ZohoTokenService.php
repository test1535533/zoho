<?php

namespace App\Services;

use App\Interfaces\Repositories\ZohoTokenRepositoryInterface;
use App\Interfaces\Services\ZohoTokenServiceInterface;
use App\Models\ZohoToken;

class ZohoTokenService implements ZohoTokenServiceInterface
{
    private ZohoTokenRepositoryInterface $zohoTokenRepository;

    /**
     * UserService constructor.
     * @param ZohoTokenRepositoryInterface $zohoTokenRepository
     */
    public function __construct(ZohoTokenRepositoryInterface $zohoTokenRepository)
    {
        $this->zohoTokenRepository = $zohoTokenRepository;
    }

    /**
     * @return ZohoToken|null
     */
    public function getToken(): ZohoToken|null
    {
        return $this->zohoTokenRepository->getToken();
    }

    /**
     * @param array $data
     * @return ZohoToken
     */
    public function createToken(array $data): ZohoToken
    {
        return $this->zohoTokenRepository->createToken($data);
    }

    /**
     * @param ZohoToken $zohoToken
     * @param array $data
     * @return ZohoToken
     */
    public function updateToken(ZohoToken $zohoToken, array $data): ZohoToken
    {
        return $this->zohoTokenRepository->updateToken($zohoToken, $data);
    }

}
