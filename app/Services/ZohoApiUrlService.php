<?php
namespace App\Services;

use App\Interfaces\Services\ZohoApiUrlServiceInterface;

class ZohoApiUrlService implements ZohoApiUrlServiceInterface
{
    /**
     * @return string
     */
    public function setToken(): string
    {
        return urldecode("https://accounts.zoho.eu/oauth/v5/token");
    }

    /**
     * @param string $refresh_token
     * @param string $client_id
     * @param string $client_secret
     * @return string
     */
    public function refreshToken(string $refresh_token, string $client_id, string $client_secret): string
    {
        return urldecode('https://accounts.zoho.eu/oauth/v2/token?refresh_token='.$refresh_token.'&client_id='.$client_id.'&client_secret='.$client_secret.'&grant_type=refresh_token');
    }

    /**
     * @return string
     */
    public function account(): string
    {
        return urldecode("https://www.zohoapis.eu/crm/v5/Accounts");
    }

    /**
     * @return string
     */
    public function deals(): string
    {
        return urldecode("https://www.zohoapis.eu/crm/v5/Deals");
    }
}
